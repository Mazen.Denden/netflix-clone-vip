//
//  Webservice.swift
//  MazenProject
//
//  Created by Mazen Denden on 8/14/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import Foundation

class Webservice {
    
    private init() {}
    static var shared = Webservice()
    
    let url = "http://www.json-generator.com/api/json/get/clBnzhMZnS"
    let urlSession = URLSession.shared
    let decoder = JSONDecoder()
    
    func getMoviesService(completion: @escaping (Result<[HomeModel], Error>) -> Void) {
        guard let url = URL(string: url) else { return }

        let dataTask = urlSession.dataTask(with: url) { data, _, error in

            if let error = error {
                completion(.failure(error))
                return
            }

            if let data = data as Data? {
                do {
                    let sections = try self.decoder.decode([HomeModel].self, from: data)
                    completion(.success(sections))
                } catch let error {
                    completion(.failure(error))
                }
            }
        }
        dataTask.resume()
    }
}
