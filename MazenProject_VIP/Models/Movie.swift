//
//  Movie.swift
//  MazenProject
//
//  Created by Mazen Denden on 8/14/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import Foundation

struct Movie: Codable {
    var identifier: Int?
    var title: String?
    var releaseYear: Int?
    var rating: Float?
    var runtime: Int?
    var summary: String?
    var coverImage: String?
    var language: String?
    var screenshots: [String] = []
    
    private enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case title
        case releaseYear = "year"
        case rating
        case runtime
        case summary
        case language
        case coverImage = "large_cover_image"
        case screenshots
    }
}

class MovieViewModel {
    private var movie = Movie()
    
    init(movie: Movie = Movie()) {
        self.movie = movie
    }
    
    var title: String { return movie.title ?? "" }
    var year: Int { return movie.releaseYear ?? 0 }
    var rating: Float { return movie.rating ?? 0 }
    var runtime: Int { return movie.runtime ?? 0 }
    var summary: String { return movie.summary ?? "" }
    var language: String {
        return movie.language == "English" ? "US" : ""
    }
    var coverImage: String { return movie.coverImage ?? "" }
    var screenshots: [String] { return movie.screenshots }

}
