//
//  Section.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/16/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import Foundation

protocol HomeViewModelDelegate: class {
    func setEvent(event: HomeViewModelEventType)
    func presentAlert(message: String)
}

struct HomeModel: Codable {
    var movies: [Movie] = []
    var type: SectionType?
    
    private enum CodingKeys: String, CodingKey {
        case type
        case movies = "data"
    }
}

class HomeViewModel {
    
    var sections: [HomeModel] = []
    var teasersArray: [HomeModel] = []
    var featuredArray: [HomeModel] = []
    var othersArray: [HomeModel] = []
    
    weak var delegate: HomeViewModelDelegate?
    
    func getMovies() {
        delegate?.setEvent(event: .showLoader)
        
        Webservice.shared.getMoviesService { [unowned self] (result) in
            self.delegate?.setEvent(event: .hideLoader)
            self.delegate?.setEvent(event: .endRefresh)
            
            DispatchQueue.main.async {
                switch result {
                case .success(let sections):
                    
                    self.sections = sections
                    
                    self.teasersArray = sections.filter { $0.type == SectionType.teasers }
                    self.featuredArray = sections.filter { $0.type == SectionType.featured }
                    self.othersArray = sections.filter { $0.type == SectionType.other }
                    
                    self.delegate?.setEvent(event: .reloadTableView)
                    
                default:
                    self.delegate?.presentAlert(message: "Failed to retrive data from server")
                    
                }
            }
        }
    }
    
}
