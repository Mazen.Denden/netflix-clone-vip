//
//  SectionType.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/17/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import Foundation

enum SectionType: String, Codable, CustomStringConvertible {
    case teasers = "Teasers"
    case featured = "Featured"
    case other = "Other"
    
    var description: String {
        switch self {
        case .teasers: return TextKeys.Home.teasers
        case .featured: return TextKeys.Home.featured
        default: return TextKeys.Home.other
        }
    }
}
