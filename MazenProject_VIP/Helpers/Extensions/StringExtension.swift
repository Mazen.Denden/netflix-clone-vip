//
//  StringExtension.swift
//  MazenProject
//
//  Created by Mazen Denden on 9/23/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import Foundation

extension String {
    func localized() -> String {
        return NSLocalizedString(self, tableName: "Localizable", value: "**\(self)**", comment: "")
    }
}
