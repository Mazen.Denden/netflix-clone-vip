//
//  ViewExtension.swift
//  MazenProject_VIP
//
//  Created by Mazen Denden on 12/1/20.
//

import UIKit

extension UIView {
    // Top Anchor
    var safeAreaTopAnchor: NSLayoutYAxisAnchor {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.topAnchor
        } else {
            return self.topAnchor
        }
    }
    
    // Bottom Anchor
    var safeAreaBottomAnchor: NSLayoutYAxisAnchor {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.bottomAnchor
        } else {
            return self.bottomAnchor
        }
    }
    
    // Left Anchor
    var safeAreaLeftAnchor: NSLayoutXAxisAnchor {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.leftAnchor
        } else {
            return self.leftAnchor
        }
    }
    
    // Right Anchor
    var safeAreaRightAnchor: NSLayoutXAxisAnchor {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.rightAnchor
        } else {
            return self.rightAnchor
        }
    }

    func anchor(topAnchor: (top: NSLayoutYAxisAnchor?, topPadding: CGFloat)? = (nil, 0),
                leftAnchor: (left: NSLayoutXAxisAnchor?, leftPadding: CGFloat)? = (nil, 0),
                rightAnchor: (right: NSLayoutXAxisAnchor?, rightPadding: CGFloat)? = (nil, 0),
                bottomAnchor: (bottom: NSLayoutYAxisAnchor?, bottomPadding: CGFloat)? = (nil, 0),
                centerXAnchor: (centerX: NSLayoutXAxisAnchor?, centerXPadding: CGFloat)? = (nil, 0),
                centerYAnchor: (centerY: NSLayoutYAxisAnchor?, centerYPadding: CGFloat)? = (nil, 0),
                width: CGFloat? = nil, height: CGFloat? = nil) {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let topAnchor = topAnchor, let top = topAnchor.top {
            self.topAnchor.constraint(equalTo: top, constant: topAnchor.topPadding).isActive = true
        }
        
        if let leftAnchor = leftAnchor, let left = leftAnchor.left {
            self.leftAnchor.constraint(equalTo: left, constant: leftAnchor.leftPadding).isActive = true
        }
        
        if let rightAnchor = rightAnchor, let right = rightAnchor.right {
            self.rightAnchor.constraint(equalTo: right, constant: -rightAnchor.rightPadding).isActive = true
        }
        
        if let bottomAnchor = bottomAnchor, let bottom = bottomAnchor.bottom {
            self.bottomAnchor.constraint(equalTo: bottom, constant: -bottomAnchor.bottomPadding).isActive = true
        }
        
        if let centerXAnchor = centerXAnchor, let centerX = centerXAnchor.centerX {
            self.centerXAnchor.constraint(equalTo: centerX, constant: centerXAnchor.centerXPadding).isActive = true
        }
        
        if let centerYAnchor = centerYAnchor, let centerY = centerYAnchor.centerY {
            self.centerYAnchor.constraint(equalTo: centerY, constant: centerYAnchor.centerYPadding).isActive = true
        }
        
        if let width = width {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
        if let height = height {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
    
    func rounded(corners: CACornerMask, cornerRadius: CGFloat) {
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
        self.layer.maskedCorners = corners
    }
    
    func fadeIn() {
        self.alpha -= 10
        self.isHidden = false
        UIView.animate(withDuration: 0.2) {
            self.alpha += 10
        }
    }
    
    func fadeOut() {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha -= 10
        }, completion: { _ in
            self.isHidden = true
            self.alpha += 10
        })
    }
}
