//
//  TextKeys.swift
//  MazenProject
//
//  Created by Mazen Denden on 9/23/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import Foundation

struct TextKeys {
    private init() {}
    
    struct Login {
        static let signupTitle = "login.signup.button.title".localized()
        static let signinTitle = "login.signin.button.title".localized()
    }
    
    struct Home {
        static let teasers = "Teasers".localized()
        static let featured = "Featured".localized()
        static let other = "Other".localized()
    }
}
