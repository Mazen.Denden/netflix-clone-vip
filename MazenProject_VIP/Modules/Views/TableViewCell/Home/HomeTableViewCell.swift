//
//  HomeTableViewCell.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/14/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import UIKit

enum ImageType {
    case circle
    case rectangle
}

class HomeTableViewCell: UITableViewCell {
    
    var didSelectMovie: ((MovieViewModel) -> Void)?
    
    var imageType = ImageType.circle
    
    let movieCellId = "movieCellId"
    var moviesViewModels: [MovieViewModel] = [] {
        didSet {
            if !moviesViewModels.isEmpty {
                moviesCollectionView.reloadData()
            }
        }
    }
    
    let moviesCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = ColorConstants.backgroundBlack
        
        return collectionView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupCell()
        setupCollectionView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCell() {
        contentView.addSubview(moviesCollectionView)
        moviesCollectionView.anchor(topAnchor: (self.topAnchor, 0),
                                    leftAnchor: (self.leftAnchor, 0),
                                    rightAnchor: (self.rightAnchor, 0),
                                    bottomAnchor: (self.bottomAnchor, 0))
    }
    
    func setupCollectionView() {
        moviesCollectionView.delegate = self
        moviesCollectionView.dataSource = self
        moviesCollectionView.isUserInteractionEnabled = true
        moviesCollectionView.register(MovieCell.self, forCellWithReuseIdentifier: movieCellId)
    }
}
