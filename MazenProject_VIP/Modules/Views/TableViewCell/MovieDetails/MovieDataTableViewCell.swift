//
//  MovieDataTableViewCell.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/24/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import UIKit

class MovieDataTableViewCell: UITableViewCell {
    
    //    Header Stack View
    let horizontalHeaderStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 20
        return stackView
    }()
    
    lazy var shareStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [shareLabel, shareImageView])
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        return stackView
    }()
    
    let shareLabel: UILabel = {
        let label = UILabel()
        label.text = "Partager"
        label.textColor = ColorConstants.backgroundRed
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    let shareImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "icShare")
        imageView.contentMode = .left
        return imageView
    }()
    
    let ratingLabel: UILabel = {
        let label = UILabel()
        label.textColor = .lightGray
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    let matchLabel: UILabel = {
        let label = UILabel()
        label.text = "80% Match"
        label.textColor = ColorConstants.backgroundRed
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    //    Data Horizonral Stack
    let horizontalDataStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 20
        return stackView
    }()
    
    let verticalYearStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 5
        return stackView
    }()
    
    let verticalCountryStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 5
        return stackView
    }()
    
    let verticalLengthStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 5
        return stackView
    }()
    
    //     Data Labels
    let yearLabel: UILabel = {
        let label = UILabel()
        label.text = "Année"
        label.textColor = .lightGray
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    let yearDataLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    let countryLabel: UILabel = {
        let label = UILabel()
        label.text = "Pays"
        label.textColor = .lightGray
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    let countryDataLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    let lengthLabel: UILabel = {
        let label = UILabel()
        label.text = "Durée"
        label.textColor = .lightGray
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    let lengthDataLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    var movieViewModel: MovieViewModel? {
        didSet {
            guard let movieVM = movieViewModel else { return }
            
            ratingLabel.text = "IMDb \(movieVM.rating)/10"
            yearDataLabel.text = "\(movieVM.year)"
            countryDataLabel.text = movieVM.language
            lengthDataLabel.text = "\(movieVM.runtime)mins"
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupCell()
    }
    
    func setupCell() {
        
        backgroundColor = .black
        selectionStyle = .none
        
        horizontalHeaderStackView.addArrangedSubview(shareStackView)
        horizontalHeaderStackView.addArrangedSubview(ratingLabel)
        horizontalHeaderStackView.addArrangedSubview(matchLabel)
        
        addSubview(horizontalHeaderStackView)
        horizontalHeaderStackView.anchor(topAnchor: (topAnchor, 8), leftAnchor: (leftAnchor, 16), rightAnchor: (rightAnchor, 16))
        
        horizontalDataStackView.addArrangedSubview(verticalYearStackView)
        horizontalDataStackView.addArrangedSubview(verticalCountryStackView)
        horizontalDataStackView.addArrangedSubview(verticalLengthStackView)
        
        addSubview(horizontalDataStackView)
        horizontalDataStackView.anchor(topAnchor: (horizontalHeaderStackView.bottomAnchor, 12), leftAnchor: (leftAnchor, 16), rightAnchor: (rightAnchor, 16), bottomAnchor: (bottomAnchor, 16))
        
        verticalYearStackView.addArrangedSubview(yearLabel)
        verticalYearStackView.addArrangedSubview(yearDataLabel)
        
        verticalCountryStackView.addArrangedSubview(countryLabel)
        verticalCountryStackView.addArrangedSubview(countryDataLabel)
        
        verticalLengthStackView.addArrangedSubview(lengthLabel)
        verticalLengthStackView.addArrangedSubview(lengthDataLabel)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
