//
//  MovieDescriptionTableViewCell.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/24/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import UIKit

class MovieDescriptionTableViewCell: UITableViewCell {
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .lightGray
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupCell()
    }
    
    func setupCell() {
        backgroundColor = .black
        selectionStyle = .none
        
        addSubview(descriptionLabel)
        descriptionLabel.anchor(topAnchor: (topAnchor, 8), leftAnchor: (leftAnchor, 8), rightAnchor: (rightAnchor, 8), bottomAnchor: (bottomAnchor, 8))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
