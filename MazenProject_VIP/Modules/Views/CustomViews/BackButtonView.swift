//
//  BackButtonView.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/24/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import UIKit

class BackButtonView: UIView {
    
    var itemsColor: UIColor = ColorConstants.backgroundRed {
        didSet {
            leftItem.tintColor = itemsColor
            rightItem.textColor = itemsColor
        }
    }
    
    var leftItemIsShown: Bool = true {
        didSet {
            leftItem.isHidden = !leftItemIsShown
            
            if !leftItemIsShown {
                NSLayoutConstraint.deactivate([
                    rightItem.leftAnchor.constraint(equalTo: leftAnchor, constant: 2 + 25 + 2)
                ])
                
                NSLayoutConstraint.activate([
                    rightItem.leftAnchor.constraint(equalTo: leftAnchor, constant: 2)
                ])
            }
        }
    }
    
    let leftItem: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "icBack").withRenderingMode(.alwaysTemplate)
        return imageView
    }()
    
    let rightItem: UILabel = {
        let label = UILabel()
        label.text = "Back"
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        backgroundColor = .clear
        
        leftItem.tintColor = itemsColor
        rightItem.textColor = itemsColor

        addSubview(leftItem)
        addSubview(rightItem)
        
        leftItem.anchor(topAnchor: (topAnchor, 2),
                        leftAnchor: (leftAnchor, 2),
                        bottomAnchor: (bottomAnchor, 2))
        rightItem.anchor(rightAnchor: (rightAnchor, 2),
                         centerYAnchor: (centerYAnchor, 0))
        rightItem.leftAnchor.constraint(equalTo: leftAnchor, constant: 2 + 25 + 2).isActive = true
    }
}
