//
//  HeaderView.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/30/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class MovieDetailsHeaderView: UIView {
    
    let headerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "backgroundImage")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let playButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.text = ""
        button.backgroundColor = .white
        button.setImage(#imageLiteral(resourceName: "icPlay"), for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(headerImageView)
        headerImageView.anchor(
            topAnchor: (topAnchor, 0),
            leftAnchor: (leftAnchor, 0),
            rightAnchor: (rightAnchor, 0)
        )
        headerImageView.rounded(
            corners: [
                .layerMinXMaxYCorner,
                .layerMaxXMaxYCorner],
            cornerRadius: 40
        )
        
        addSubview(playButton)
        playButton.anchor(
            topAnchor: (headerImageView.bottomAnchor, -25),
            rightAnchor: (rightAnchor, 25),
            bottomAnchor: (bottomAnchor, 0),
            width: 50, height: 50
        )
        playButton.layer.cornerRadius = 25
        
    }
    
    func setupHeaderImage(imageString: String) {
        DispatchQueue.main.async {
            guard let imageUrl = URL(string: imageString) else { return }
            
            self.setKingFisherImage(withUrl: imageUrl)
        }
    }
    
    func setKingFisherImage(withUrl url: URL) {
        headerImageView.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholderImage"),
            options: [
                .cacheOriginalImage,
                .transition(.fade(1))
            ]
        )
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
