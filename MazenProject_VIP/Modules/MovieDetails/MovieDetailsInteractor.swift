//
//  MovieDetailsInteractor.swift
//  MazenProject_VIP
//
//  Created by Mazen Denden on 12/2/20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol MovieDetailsInteractor {
    var movieViewModel: MovieViewModel? { get }
    func viewDidLoad()
}

class MovieDetailsInteractorImplementation: MovieDetailsInteractor {
    var presenter: MovieDetailsPresenter?

    var movieViewModel: MovieViewModel?
    
    func viewDidLoad() {
        if let item = movieViewModel {
            presenter?.interactor(didRetrieveItem: item)
        }
    }
}
