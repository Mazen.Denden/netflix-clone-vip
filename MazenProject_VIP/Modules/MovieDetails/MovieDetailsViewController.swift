//
//  MovieDetailsViewController.swift
//  MazenProject_VIP
//
//  Created by Mazen Denden on 12/2/20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol MovieDetailsViewControllerOutput: class {
    func presenter(didRetrieveItem item: MovieViewModel)
}

class MovieDetailsViewController: BaseViewController {
    var movieDetailsView: MovieDetailsView?
    var interactor: MovieDetailsInteractor?
    var router: MovieDetailsRouter?
    
    // MARK: Lifecycle
    override func loadView() {
        super.loadView()
        self.view = movieDetailsView
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationBar()
        interactor?.viewDidLoad()
    }
    
    func setupNavigationBar() {
        backButtonView.itemsColor = .white
    }
}

extension MovieDetailsViewController: MovieDetailsViewControllerOutput {
    func presenter(didRetrieveItem item: MovieViewModel) {
        backButtonView.rightItem.text = item.title
        
        movieDetailsView?.updateView(with: item)
    }
}
