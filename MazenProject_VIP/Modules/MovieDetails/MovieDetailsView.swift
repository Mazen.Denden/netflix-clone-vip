//
//  MovieDetailsView.swift
//  MazenProject_VIP
//
//  Created by Mazen Denden on 12/2/20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

let dataCellId = "dataCell"
let descriptionCellId = "descriptionCell"
let screenshotsCellId = "screenshotsCell"

private let headerHeight = UIScreen.main.bounds.height * 0.43
private let headerWidth = UIScreen.main.bounds.width

class MovieDetailsView: UIView {

    let dataTableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .black
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.separatorColor = .white
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 50)
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()
    
    var movieViewModel: MovieViewModel? {
        didSet {
            setupHeader()
        }
    }
    var header: MovieDetailsHeaderView?
    
    enum MovieDetailCellType: CaseIterable {
        case data
        case description
        case screenshots
        
        init(row: Int) {
            switch row {
            case 0: self = .data
            case 1: self = .description
            default: self = .screenshots
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
        setupTableView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupUI() {
        addSubview(dataTableView)
        
        dataTableView.anchor(topAnchor: (topAnchor, 0),
                             leftAnchor: (leftAnchor, 0),
                             rightAnchor: (rightAnchor, 0),
                             bottomAnchor: (bottomAnchor, 0))
    }
    
    private func setupTableView() {
        dataTableView.delegate = self
        dataTableView.dataSource = self
        
        dataTableView.register(MovieDataTableViewCell.self, forCellReuseIdentifier: dataCellId)
        dataTableView.register(MovieDescriptionTableViewCell.self, forCellReuseIdentifier: descriptionCellId)
        dataTableView.register(MovieScreenshotsTableViewCell.self, forCellReuseIdentifier: screenshotsCellId)

        dataTableView.tableFooterView = UIView()
        
        dataTableView.tableHeaderView = nil
        dataTableView.contentInset = UIEdgeInsets(top: headerHeight, left: 0, bottom: 0, right: 0)
    }

    private func setupHeader() {
        header = MovieDetailsHeaderView(frame: CGRect(x: 0, y: 0, width: headerWidth, height: headerHeight))
        header?.setupHeaderImage(imageString: self.movieViewModel?.coverImage ?? "")
        dataTableView.addSubview(header ?? UIView())
    }
    
    public func updateView(with viewModel: MovieViewModel) {
        self.movieViewModel = viewModel
    }
}

extension MovieDetailsView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MovieDetailCellType.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType = MovieDetailCellType(row: indexPath.row)
        
        switch cellType {
        case .data:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: dataCellId, for: indexPath) as? MovieDataTableViewCell else { return UITableViewCell() }
            cell.movieViewModel = movieViewModel
            return cell
        case .description:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: descriptionCellId, for: indexPath) as? MovieDescriptionTableViewCell else { return UITableViewCell() }
            cell.descriptionLabel.text = movieViewModel?.summary
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: screenshotsCellId, for: indexPath) as? MovieScreenshotsTableViewCell else { return UITableViewCell() }
            cell.imagesArray = movieViewModel?.screenshots ?? []
            return cell
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if header != nil {
            let yPos: CGFloat = -scrollView.contentOffset.y
            
            if yPos > 0 {
                var headerRect: CGRect = header?.frame ?? CGRect()
                
                headerRect.origin.y = -yPos
                headerRect.size.height = yPos
                
                header?.frame = headerRect

                self.dataTableView.sectionHeaderHeight = headerRect.size.height
            }
        }
    }
    
}
