//
//  MovieDetailsConfigurator.swift
//  MazenProject_VIP
//
//  Created by Mazen Denden on 12/2/20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class MovieDetailsConfigurator {
    static func configureModule(movieViewModel: MovieViewModel, viewController: MovieDetailsViewController) {
        let view = MovieDetailsView()
        let interactor = MovieDetailsInteractorImplementation()
        let presenter = MovieDetailsPresenterImplementation()
        
        viewController.movieDetailsView = view
        viewController.interactor = interactor
        
        interactor.presenter = presenter
        interactor.movieViewModel = movieViewModel
        
        presenter.viewController = viewController
    }
}
