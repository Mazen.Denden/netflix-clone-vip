//
//  MovieDetailsPresenter.swift
//  MazenProject_VIP
//
//  Created by Mazen Denden on 12/2/20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation

protocol MovieDetailsPresenter: class {
    func interactor(didRetrieveItem item: MovieViewModel)
}

class MovieDetailsPresenterImplementation: MovieDetailsPresenter {
    weak var viewController: MovieDetailsViewController?
    
    func interactor(didRetrieveItem item: MovieViewModel) {
        viewController?.presenter(didRetrieveItem: item)
    }
}
