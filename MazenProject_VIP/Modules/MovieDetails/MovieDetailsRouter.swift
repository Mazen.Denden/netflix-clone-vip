//
//  MovieDetailsRouter.swift
//  MazenProject_VIP
//
//  Created by Mazen Denden on 12/2/20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol MovieDetailsRouter: class {
    var navigationController: UINavigationController? { get }
    
    // func routeToDetail(with id: String)
}

class MovieDetailsRouterImplementation: MovieDetailsRouter {
    weak var navigationController: UINavigationController?
    
    // func routeToDetail(with id: String) {
    //     let viewController = MovieDetailsViewController()
    //     MovieDetailsConfigurator.configureModule(viewController: viewController)

    //     navigationController?.pushViewController(viewController, animated: true)
    // }
}