//
//  BaseViewController.swift
//  MazenProject_VIP
//
//  Created by Mazen Denden on 12/1/20.
//

import UIKit

enum ViewContext {
    case singup
    case loggedIn
}

class BaseViewController: UIViewController {
    
    let backButtonView = BackButtonView()
    
    var hasBackAction: Bool = true {
        didSet {
            backButtonView.leftItemIsShown = hasBackAction
        }
    }
    
    lazy var backButton: UIBarButtonItem = {
        let button = UIBarButtonItem()
        button.customView = backButtonView
        button.customView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleBackAction(sender:))))
        return button
    }()
    
    var context = ViewContext.loggedIn {
        didSet {
            setupClassName()
        }
    }
    
    var activeField: UITextField?
    
    // appDelegate instance
    weak var appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        setUpNavigationItems()
        setupClassName()
        
        setupRightSwipe()
    }
    
    func setupRightSwipe() {
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleRightSwipe(sender:)))
        view.addGestureRecognizer(rightSwipe)
    }
    
    @objc func handleRightSwipe(sender: UISwipeGestureRecognizer) {
        if sender.state == .ended {
            navigationController?.popViewController(animated: true)
        }
    }
    
    func setUpNavigationItems() {
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationController?.navigationBar.barStyle = .black
        
        self.backButtonView.itemsColor = .red
    }
    
    @objc func handleBackAction(sender: UIBarButtonItem) {
        if hasBackAction {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func setupClassName() {
        let className = StringUtils.className(self)
        
        switch context {
        case .singup:
            backButtonView.rightItem.text = ""
            self.title = className.localized()
            
        default:
            backButtonView.rightItem.text = className.localized()
            self.title = ""
        }
        
    }
}
