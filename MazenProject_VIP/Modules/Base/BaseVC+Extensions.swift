//
//  BaseVC+Extensions.swift
//  MazenProject
//
//  Created by Mazen Denden on 10/23/19.
//  Copyright © 2019 Mazen Denden. All rights reserved.
//

import UIKit

extension BaseViewController: UIGestureRecognizerDelegate {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.delegate = self
        tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
}

extension BaseViewController {
    func showSimpleAlert(title: String? = nil, message: String, buttonTitle: String? = "ok", action: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: action))
        
        self.present(alert, animated: true)
    }
    
    func showAlert(title: String? = nil, message: String, cancelButtonTitle: String, acceptButtonTitle: String, anctionHandler: @escaping (Int) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .default) { (_) in
            anctionHandler(1)
        }
        let acceptAction = UIAlertAction(title: cancelButtonTitle, style: .default) { (_) in
            anctionHandler(2)
        }
        
        alert.addAction(cancelAction)
        alert.addAction(acceptAction)
        
        self.present(alert, animated: true)
    }
}
