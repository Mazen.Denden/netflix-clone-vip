//
//  HomeView.swift
//  MazenProject_VIP
//
//  Created by Mazen Denden on 12/1/20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

let homeTableViewCellId = "homeTableViewCellId"

class HomeView: UIView {
    
    let moviesTableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = ColorConstants.backgroundBlack
        tableView.separatorStyle = .none
        return tableView
    }()
    
    let refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = ColorConstants.backgroundRed
        return refreshControl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupUI()
        setupTableView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupUI() {
        moviesTableView.refreshControl = refreshControl
        
        moviesTableView.tableFooterView = UIView()
        
        addSubview(moviesTableView)
        moviesTableView.anchor(topAnchor: (safeAreaTopAnchor, 12),
                               leftAnchor: (safeAreaLeftAnchor, 12),
                               rightAnchor: (safeAreaRightAnchor, 12),
                               bottomAnchor: (safeAreaBottomAnchor, 12))
    }

    private func setupTableView() {
        moviesTableView.isUserInteractionEnabled = true
        moviesTableView.register(HomeTableViewCell.self, forCellReuseIdentifier: homeTableViewCellId)
    }
    
    func reloadTableView() {
        DispatchQueue.main.async {
            self.moviesTableView.reloadData()
        }
    }
    
    func showLoader() {
        DispatchQueue.main.async {
            if !self.refreshControl.isRefreshing {
                Loader.showLoader()
            }
        }
    }
    
    func hideLoader() {
        DispatchQueue.main.async {
            Loader.hideLoader()
        }
    }
    
    func endRefreshing() {
        DispatchQueue.main.async {
            self.refreshControl.endRefreshing()
        }
    }
}
