//
//  HomeService.swift
//  MazenProject_VIP
//
//  Created by Mazen Denden on 12/1/20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation

protocol HomeService: class {
    func getMovies(completion:  @escaping ([HomeModel]?) -> Void)
}

protocol HomeServiceDelegate: AnyObject {
    func setEvent(event: HomeViewModelEventType)
    func presentAlert(message: String)
}

class HomeServiceImplementation: HomeService {
    weak var delegate: HomeServiceDelegate?
    
    func getMovies(completion: @escaping ([HomeModel]?) -> Void) {
        delegate?.setEvent(event: .showLoader)
        
        Webservice.shared.getMoviesService { result in
            self.delegate?.setEvent(event: .hideLoader)
            self.delegate?.setEvent(event: .endRefresh)
            
            switch result {
            case .success(let homeModels):
                completion(homeModels)
                
                self.delegate?.setEvent(event: .reloadTableView)
                
            case .failure(let error):
                completion(nil)
                self.delegate?.presentAlert(message: "Failed to retrive data from server \(error)")
            }
        }
    }
}
