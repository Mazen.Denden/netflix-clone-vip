//
//  HomePresenter.swift
//  MazenProject_VIP
//
//  Created by Mazen Denden on 12/1/20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation

protocol HomePresenter: AnyObject {
    func interactor(didRetrieveItems items: [HomeModel])
    func interactor(didFindItem item: MovieViewModel)
}

class HomePresenterImplementation: HomePresenter {
    weak var viewController: HomeViewController?
    
    func interactor(didRetrieveItems items: [HomeModel]) {
        let homeViewModel = HomeViewModel()
        homeViewModel.sections = items
        homeViewModel.teasersArray = items.filter { $0.type == SectionType.teasers }
        homeViewModel.featuredArray = items.filter { $0.type == SectionType.featured }
        homeViewModel.othersArray = items.filter { $0.type == SectionType.other }
        
        viewController?.presenter(didRetrieveItem: homeViewModel)
    }
    
    func interactor(didFindItem item: MovieViewModel) {
        viewController?.presenter(didRetrieveSelectedItem: item)
    }
}
