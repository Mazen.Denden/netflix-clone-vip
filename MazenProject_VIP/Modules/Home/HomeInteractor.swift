//
//  HomeInteractor.swift
//  MazenProject_VIP
//
//  Created by Mazen Denden on 12/1/20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol HomeInteractor {
    func viewDidLoad()
    func didSelectItem(item: MovieViewModel)
}

class HomeInteractorImplementation: HomeInteractor {
    var presenter: HomePresenter?
    var apiWorker: HomeService?
    
    private var homeModels: [HomeModel] = []
    
    func viewDidLoad() {
        apiWorker?.getMovies(completion: { (homeModels) in
            if let homeModels = homeModels {
                self.homeModels = homeModels
                
                self.presenter?.interactor(didRetrieveItems: homeModels)
            }
        })
    }
    
    func didSelectItem(item: MovieViewModel) {
        presenter?.interactor(didFindItem: item)
    }
}
