//
//  HomeRouter.swift
//  MazenProject_VIP
//
//  Created by Mazen Denden on 12/1/20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol HomeRouter: AnyObject {
    var navigationController: UINavigationController? { get }
    
    func routeToDetail(with movie: MovieViewModel)
}

class HomeRouterImplementation: HomeRouter {
    weak var navigationController: UINavigationController?
    
    func routeToDetail(with movie: MovieViewModel) {
        let viewController = MovieDetailsViewController()
        MovieDetailsConfigurator.configureModule(movieViewModel: movie, viewController: viewController)
        
        navigationController?.pushViewController(viewController, animated: true)
    }
}
