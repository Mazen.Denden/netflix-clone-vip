//
//  HomeConfigurator.swift
//  MazenProject_VIP
//
//  Created by Mazen Denden on 12/1/20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class HomeConfigurator {
    static func configureModule(viewController: HomeViewController) {
        let view = HomeView()
        let interactor = HomeInteractorImplementation()
        let presenter = HomePresenterImplementation()
        let service = HomeServiceImplementation()
        let router = HomeRouterImplementation()
        
        viewController.homeView = view
        viewController.interactor = interactor
        viewController.router = router
        
        interactor.presenter = presenter
        interactor.apiWorker = service
        
        presenter.viewController = viewController
        
        router.navigationController = viewController.navigationController
        
        service.delegate = viewController
    }
}
