//
//  HomeViewController.swift
//  MazenProject_VIP
//
//  Created by Mazen Denden on 12/1/20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol HomeViewControllerOutput: AnyObject {
    func presenter(didRetrieveItem item: HomeViewModel)
    func presenter(didRetrieveSelectedItem item: MovieViewModel)
}

class HomeViewController: BaseViewController {
    var homeView: HomeView?
    var interactor: HomeInteractor?
    var router: HomeRouter?

    var homeViewModel: HomeViewModel?
    
    // MARK: Lifecycle
    override func loadView() {
        super.loadView()
        self.view = homeView
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        homeView?.moviesTableView.delegate = self
        homeView?.moviesTableView.dataSource = self
        
        setupNavigationBar()
        interactor?.viewDidLoad()
    }
    
    private func setupNavigationBar() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
        
        self.navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "nLogo"))
    }
    
    func didSelectMovie(movieViewModel: MovieViewModel) {
        interactor?.didSelectItem(item: movieViewModel)
    }
}

extension HomeViewController: HomeViewControllerOutput {
    func presenter(didRetrieveItem item: HomeViewModel) {
        self.homeViewModel = item
        self.homeView?.reloadTableView()
    }
    
    func presenter(didRetrieveSelectedItem item: MovieViewModel) {
        router?.routeToDetail(with: item)
    }
}

extension HomeViewController: HomeServiceDelegate {
    func setEvent(event: HomeViewModelEventType) {
        switch event {
        case .reloadTableView:
            self.homeView?.reloadTableView()
            
        case .showLoader:
            self.homeView?.showLoader()
            
        case .hideLoader:
            self.homeView?.hideLoader()
            
        case .endRefresh:
            self.homeView?.endRefreshing()
        }
    }
    
    func presentAlert(message: String) {
        DispatchQueue.main.async {
            self.showSimpleAlert(message: message)
        }
    }
    
}

private let headerSectionHeight: CGFloat = 112
private let sectionHeight: CGFloat = 140
private let headerHeight = UIScreen.main.bounds.height * 0.055

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return homeViewModel?.sections.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionType = homeViewModel?.sections[section].type
        
        switch sectionType {
        case .teasers: return homeViewModel?.teasersArray.count ?? 0
        case .featured: return homeViewModel?.featuredArray.count ?? 0
        default: return homeViewModel?.othersArray.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: homeTableViewCellId, for: indexPath) as? HomeTableViewCell else { return UITableViewCell() }
        guard let homeViewModel = homeViewModel else { return UITableViewCell() }
        
        var moviesViewModelArray: [MovieViewModel] = []
        
        let sectionType = homeViewModel.sections[indexPath.section].type
        
        switch sectionType {
        case .teasers:
            cell.imageType = .circle
            moviesViewModelArray = homeViewModel.teasersArray[indexPath.row].movies.map { return MovieViewModel(movie: $0) }
            
        case .featured:
            cell.imageType = .rectangle
            moviesViewModelArray = homeViewModel.featuredArray[indexPath.row].movies.map { return MovieViewModel(movie: $0) }
            
        default:
            cell.imageType = .rectangle
            moviesViewModelArray = homeViewModel.othersArray[indexPath.row].movies.map { return MovieViewModel(movie: $0) }
        }
        
        cell.moviesViewModels = moviesViewModelArray
        cell.didSelectMovie = self.didSelectMovie
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let sectionType = homeViewModel?.sections[indexPath.section].type
        
        switch sectionType {
        case .teasers: return headerSectionHeight
        default: return sectionHeight
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return homeViewModel?.sections[section].type?.description
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionType = homeViewModel?.sections[section].type
        
        let header: HomeHeaderView = {
            let view = HomeHeaderView(frame: CGRect(x: 0,
                                                    y: 0,
                                                    width: tableView.frame.width,
                                                    height: sectionType == .teasers ? headerHeight : headerSectionHeight))
            view.backgroundColor = ColorConstants.backgroundBlack
            return view
        }()
        
        header.title = sectionType?.description ?? ""
        
        return header
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        interactor?.viewDidLoad()
    }
    
}
